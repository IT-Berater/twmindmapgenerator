package de.wenzlaff.mindmap.be;

import java.nio.file.Path;
import java.text.DateFormat;
import java.util.Objects;

/**
 * Eine Mindmap BE.
 * 
 * @author Thomas Wenzlaff
 */
public class Mindmap implements Comparable<Mindmap> {

	private Path path;

	public Path getPath() {
		return path;
	}

	public void setPath(Path path) {
		this.path = path;
	}

	public String getErstelldatum() {
		return DateFormat.getInstance().format(getPath().toFile().lastModified());
	}

	/**
	 * LIefert den Namen der Mindmap Datei.
	 * 
	 * @return der Name der Datei.
	 */
	public String getName() {
		return getPath().getFileName().toString();
	}

	/**
	 * Liefert die Größe der Mindmap Datei in kB (1000).
	 * 
	 * Im Template Aufruf z.B.: (${mindmap.size} kB)
	 * 
	 * @return die Größe der Datei in kB.
	 */
	public String getSize() {
		return "" + getPath().toFile().length() / 1000;
	}

	@Override
	public int hashCode() {
		return Objects.hash(path);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mindmap other = (Mindmap) obj;
		return Objects.equals(path, other.path);
	}

	/**
	 * Der Name der Mindmap ohne Prefix.
	 */
	@Override
	public String toString() {
		return getName().substring(0, getName().length() - 5);
	}

	/**
	 * Zum sortieren von A-Z nach Namen.
	 */
	@Override
	public int compareTo(Mindmap mindmap) {
		return getName().compareTo(mindmap.getName());
	}
}