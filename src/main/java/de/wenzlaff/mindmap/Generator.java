package de.wenzlaff.mindmap;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.wenzlaff.mindmap.be.Mindmap;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

/**
 * Generator zum erzeugen von Mindmap Info Seiten.
 * 
 * Mit Freemarker Templates.
 * 
 * http://freemarker.org/
 * 
 * Basis:
 * 
 * http://freemarker.org/docs/pgui_quickstart_all.html
 * 
 * Apache Projekt infos: http://incubator.apache.org/projects/freemarker.html
 * 
 * Online FreeMarker Template Tester: http://try.freemarker.org/
 * 
 * Quick overview (cheat sheet)
 * http://freemarker.org/docs/dgui_template_exp.html#exp_cheatsheet
 * 
 * @author Thomas Wenzlaff
 */
public class Generator {

	private static final String HILFE_TEXT = "Keine Parameter übergeben. Aufruf: de.wenzlaff.mindmap.Generator [Ausgabe Datei Name HTML] [Mindmap Template Name] [Mindmap input Verzeichnis Pfad] [Konfig Pfad]";

	private static final String ITHOUGHT_EXTENSION = ".itmz";

	private static final String CONFLICT_DATEINAME = "conflict";

	/**
	 * Generiert für die Input Verzeichnise mit dem Freemarker Template eine Ausgabe
	 * Datei im HTML Format.
	 * 
	 * @param ausgabeHtmlDateiName der zu erzeugende HTML Dateiname
	 * @param mindmapTemplate      der Name des Freemarker Template
	 * @param inputVerzeichnise    alle Input Verzeichnisse wo Mindmaps gesucht
	 *                             werden
	 * @param configuration        Konfiguration
	 * @return die Anzahl der bearbeiteten Mindmaps
	 */
	public int generate(String ausgabeHtmlDateiName, String mindmapTemplate, List<String> inputVerzeichnise, Configuration configuration) {

		checkParameter(ausgabeHtmlDateiName, mindmapTemplate, inputVerzeichnise, configuration);

		List<Mindmap> mindmaps = new ArrayList<>();

		inputVerzeichnise.forEach(inputVerzeichnis -> mindmaps.addAll(getMindmaps(inputVerzeichnis)));

		// Sortieren der Mindmaps von A-Z
		Collections.sort(mindmaps);

		generateHtml(ausgabeHtmlDateiName, mindmapTemplate, mindmaps, configuration);

		return mindmaps.size();
	}

	private void generateHtml(String ausgabeHtmlDateiName, String mindmapTemplate, List<Mindmap> inputMindmaps, Configuration configuration) {

		// Datenmodell anlegen für die Nutzung im Template
		Map<String, Object> datenModell = new HashMap<>();
		datenModell.put("user", "Thomas Wenzlaff");
		datenModell.put("mindmaps", inputMindmaps);
		datenModell.put("datum", new Date());

		try {
			// das Mindmap Template holen, es wird gecached
			Template template = configuration.getTemplate(mindmapTemplate);

			// Datenmodel mit dem Template zusammenfügen und in der Konsole und
			// Ergebnis.html ausgeben
			Writer outputWriter = new OutputStreamWriter(System.out);
			template.process(datenModell, outputWriter);

			try (Writer fileWriter = new FileWriter(new File(ausgabeHtmlDateiName))) {

				template.process(datenModell, fileWriter);
			}

		} catch (TemplateException | IOException e) {
			System.err.println("Fehler im Template. " + mindmapTemplate + " " + e.getMessage());
		}
	}

	private void checkParameter(String ausgabeHtmlDateiName, String mindmapTemplate, List<String> inputMindmaps, Configuration configuration) {
		if (ausgabeHtmlDateiName == null || ausgabeHtmlDateiName.isEmpty() || mindmapTemplate == null || mindmapTemplate.isEmpty() || inputMindmaps == null
				|| inputMindmaps.isEmpty() || configuration == null) {

			throw new IllegalArgumentException(HILFE_TEXT);
		}
	}

	private List<Mindmap> getMindmaps(String mindmapInputPath) {

		List<Mindmap> mindmaps = new ArrayList<>();

		// Mindmap Verzeichnis einlesen
		List<Path> maps = getIThoughtMindmaps(mindmapInputPath);

		if (maps.isEmpty()) {
			System.err.println("Keine Dateien im Verzeichnis: " + mindmapInputPath);
		} else {
			for (Path map : maps) {
				Mindmap m = getMindmap(map);

				if (mindmaps.contains(m)) {
					System.err.println("Die " + m + " Mindmap ist schon vorhanden in " + map);
				}
				mindmaps.add(m);
			}
		}
		return mindmaps;
	}

	private Mindmap getMindmap(Path map) {
		Mindmap m = new Mindmap();
		m.setPath(map);
		return m;
	}

	/**
	 * Liefert die Konfiguration. Dies muss nur einmal (Singelton) aufgerufen
	 * werden.
	 * 
	 * @param configPath der Path zu der Konfiguration
	 * @return die Konfiguration
	 */
	public Configuration getKonfiguration(String configPath) {
		// einmalig die Konfiguration anlegen
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_29);
		try {
			cfg.setDirectoryForTemplateLoading(new File(configPath));
			cfg.setDefaultEncoding("UTF-8");
			cfg.setLocale(Locale.GERMANY);
			cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
			cfg.setLogTemplateExceptions(false);
			cfg.setWrapUncheckedExceptions(true);
		} catch (IOException e) {
			System.err.println("Fehler beim laden der Template Konfiguration. " + configPath + " " + e.getMessage());
		}
		return cfg;
	}

	private static List<Path> getIThoughtMindmaps(String verzeichnis) {
		List<Path> fileNames = new ArrayList<>();
		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(verzeichnis))) {
			for (Path path : directoryStream) {
				String dateiPfad = path.toString();
				if (isIThoughtDatei(dateiPfad)) {
					fileNames.add(path);
				}
			}
		} catch (IOException ex) {
			System.err.println(ex.getMessage());
		}
		return fileNames;
	}

	private static boolean isIThoughtDatei(String dateiPfad) {
		return dateiPfad.endsWith(ITHOUGHT_EXTENSION) && !dateiPfad.contains(CONFLICT_DATEINAME);
	}
}