package de.wenzlaff.mindmap;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import de.wenzlaff.mindmap.be.Mindmap;
import freemarker.template.Configuration;

/**
 * Testklasse.
 * 
 * @author Thomas Wenzlaff
 */
@TestMethodOrder(OrderAnnotation.class)
public class GeneratorTest {

	private static final String USER_VERZEICHNIS = System.getProperty("user.dir");

	/** Da wo alle Mindmaps liegen. */
	private static final String MINDMAPS_BASIS_VERZEICHNIS = "/Users/thomaswenzlaff/Documents/Mindmaps";

	/** Die Verzeichnise mit den Mindmaps, die eingelesen werden. */
	private static final String[] MINDMAP_INPUT_VERZEICHNISSE = { //
			// USER_VERZEICHNIS + "/src/test/resources/mindmaps", // aus TEST
			// PROJEKT
			// "/Users/thomaswenzlaff/mindmap/Mindmap-private", // PRIVATE
			MINDMAPS_BASIS_VERZEICHNIS + "/Mindmap-2011", //
			MINDMAPS_BASIS_VERZEICHNIS + "/Mindmap-2012", //
			MINDMAPS_BASIS_VERZEICHNIS + "/Mindmap-2013", //
			MINDMAPS_BASIS_VERZEICHNIS + "/Mindmap-2014", //
			MINDMAPS_BASIS_VERZEICHNIS + "/Mindmap-2015", //
			MINDMAPS_BASIS_VERZEICHNIS + "/Mindmap-2016", //
			MINDMAPS_BASIS_VERZEICHNIS + "/Mindmap-2017", //
			MINDMAPS_BASIS_VERZEICHNIS + "/Mindmap-2018", //
			MINDMAPS_BASIS_VERZEICHNIS + "/Mindmap-2019", //
			MINDMAPS_BASIS_VERZEICHNIS + "/Mindmap-2020", //
			MINDMAPS_BASIS_VERZEICHNIS + "/Mindmap-2021", //
			MINDMAPS_BASIS_VERZEICHNIS + "/Mindmap-2022", //
			MINDMAPS_BASIS_VERZEICHNIS + "/Mindmap-2023" //
	};

	// alle 1.509 Maps

	/** Das Config Verzeichnis für die Templates. */
	private static final String CONFIG_DIR = USER_VERZEICHNIS + "/src/main/resources";

	private static final String MINDMAP_TEMPLATE = "mindmap.ftlh";

	private static final String ERGEBNIS_HTML = "ergebnis.html";

	@Test
	@DisplayName("Schreibt eine HTML Seite mit FreeMarker Generator")
	public void generate_Html_Seite() {

		List<String> verzeichnise = new ArrayList<>(Arrays.asList(MINDMAP_INPUT_VERZEICHNISSE));

		Generator generator = new Generator();
		Configuration cfg = generator.getKonfiguration(CONFIG_DIR);

		generator.generate(ERGEBNIS_HTML, MINDMAP_TEMPLATE, verzeichnise, cfg);
	}

	@Disabled
	public void streamForEachVsForCollectionTest() {

		List<Mindmap> maps = getTestMindmaps();

		System.out.println("Altes iterieren mit for Schleife:");
		List<Mindmap> ergebnis = new ArrayList<>();
		// in for-Schleife Ergebnis anzeigen und Ergebnis der Liste hinzufügen
		for (Mindmap mindmap : maps) {
			System.out.println(mindmap);
			ergebnis.add(mindmap);
		}
		assertArrayEquals(maps.toArray(), ergebnis.toArray());
		ergebnis.clear();

		System.out.println("Neu mit Stream API von Java 8 iterieren:");
		// iterieren mit forEach über eine Collection von Mindmaps und Lambda
		// expressions
		maps.stream().forEach(mindmap -> {
			System.out.println(mindmap);
			ergebnis.add(mindmap);
		});

		assertArrayEquals(maps.toArray(), ergebnis.toArray());
	}

	@Disabled
	public void streamForEachFilterTest() {

		List<Mindmap> maps = getTestMindmaps();
		assertEquals(3, maps.size());

		List<Mindmap> ergebnis = new ArrayList<>();

		System.out.println("Neu mit Stream API von Java 8 iterieren inkl. Filter:");
		// iterieren mit forEach über eine Collection von Mindmaps und Lambda
		// expressions inkl. Filter
		maps.stream() // der Stream
				.filter(mindmap -> mindmap.getName().contains("zwei"))
				// mit dem Filter,alle Mindmaps die zwei im Namen enthalten
				.forEach(mindmap -> { // über die gefundenen gefilterteten
										// Mindmaps
					System.out.println("Mindmap die (zwei) enthält: " + mindmap);
					ergebnis.add(mindmap);
				});

		assertEquals(1, ergebnis.size());
	}

	@Disabled
	public void streamForEachSortierungTest() {

		List<Mindmap> maps = getTestMindmaps();
		assertEquals(3, maps.size());

		List<Mindmap> ergebnis = new ArrayList<>();

		System.out.println("Neu mit Stream API von Java 8 sortieren:");
		// iterieren mit forEach über eine Collection von Mindmaps und Lambda
		// expressions inkl. Filter
		maps.stream() // der Stream

				.sorted() // mit natürlicher A-Z Ordnung sortieren
				.forEach(mindmap -> { // über die gefundenen gefilterteten
										// Mindmaps
					System.out.println("Mindmap: " + mindmap);
					ergebnis.add(mindmap);
				});

		assertEquals(maps.size(), ergebnis.size());
	}

	private List<Mindmap> getTestMindmaps() {
		// Liste mit Mindmaps erstellen
		List<Mindmap> maps = new ArrayList<>();

		// Mindmap 1 erstellen und der Liste hinzufügen
		Mindmap m1 = new Mindmap();
		m1.setPath(Paths.get("mindmap-eins.itmz"));
		maps.add(m1);

		// Mindmap 2 erstellen und der Liste hinzufügen
		Mindmap m2 = new Mindmap();
		m2.setPath(Paths.get("mindmap-zwei.itmz"));
		maps.add(m2);

		// Mindmap 3 erstellen und der Liste hinzufügen
		Mindmap m3 = new Mindmap();
		m3.setPath(Paths.get("mindmap-drei.itmz"));
		maps.add(m3);
		return maps;
	}
}