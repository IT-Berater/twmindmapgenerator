<map version="0.9.0">
	<attribute_registry SHOW_ATTRIBUTES="hide" />
	<node ID="CAB5E2BA-6D49-4295-9C85-9DF30449F7B8" TEXT="Demut"
		LINK="http://de.m.wikipedia.org/wiki/Demut" STYLE="bubble"
		FOLDED="false" CREATED="1428765058000" MODIFIED="1428765181000">
		<font SIZE="96" />
		<icon BUILTIN="itmz-arrow-down-red" />
		<edge COLOR="#B2B2FE" />
		<node ID="A960173C-E84E-44AE-B366-8F60935EC061"
			TEXT="Niedriggesinntsein" STYLE="bubble" FOLDED="false"
			POSITION="right" CREATED="1428765058000" MODIFIED="1428765058000">
			<font SIZE="55" />
			<edge COLOR="#FFB2FF" />
		</node>
		<node ID="0BAE44B8-7FB5-4680-8A23-177828704FD5"
			TEXT="Fehlen von Stolz" STYLE="bubble" FOLDED="false"
			POSITION="right" CREATED="1428765058000" MODIFIED="1428765058000">
			<font SIZE="36" />
			<edge COLOR="#CCB2FF" />
			<node ID="A9FF3358-2D39-4CE4-854E-11CB76B243F0" TEXT="Arroganz"
				STYLE="bubble" FOLDED="false" CREATED="1428765058000"
				MODIFIED="1428765058000">
				<font SIZE="26" />
				<edge COLOR="#CCB2FF" />
			</node>
		</node>
		<node ID="8F793D15-ECDA-4A3D-9D26-881555B92818"
			TEXT="[althochdeutsch][1] diomuoti (&#x201a;dienstwillig&#x2018;, also eigentlich &#x201a;Gesinnung eines Dienenden&#x2018;)&#10;&#10;[1]: http://de.m.wikipedia.org/wiki/Althochdeutsch"
			STYLE="bubble" FOLDED="false" POSITION="right"
			CREATED="1428765196000" MODIFIED="1428765196000">
			<font SIZE="33" />
			<edge COLOR="#B2CCFF" />
		</node>
		<node ID="9D15BC72-5FA5-4472-89C6-255C9FCFACE9"
			TEXT="[lat.][1] *humilitas*&#10;&#10;[1]: http://de.m.wikipedia.org/wiki/Lateinische_Sprache"
			STYLE="bubble" FOLDED="false" POSITION="right"
			CREATED="1428765216000" MODIFIED="1428765435000">
			<font SIZE="40" />
			<edge COLOR="#B2FFFF" />
		</node>
		<node ID="5E84A102-1233-423C-88C1-D903DA1077F1"
			TEXT="**Synonyme**" STYLE="bubble" FOLDED="false" POSITION="right"
			CREATED="1514195112000" MODIFIED="1514195112000">
			<font SIZE="47" />
			<edge COLOR="#B2FFCC" />
			<node ID="6E2BE625-1CE7-41BA-AD2B-0433D909A3CD"
				TEXT="Bescheiden" STYLE="bubble" FOLDED="false"
				CREATED="1428765569000" MODIFIED="1428765588000">
				<font SIZE="39" />
				<edge COLOR="#B2FFCC" />
			</node>
			<node ID="187A7E1D-4BF3-46D0-A2DF-43E60F2FDAE1"
				TEXT="**Gen&#xfc;gsamkeit**" STYLE="bubble" FOLDED="false"
				CREATED="1514195121000" MODIFIED="1514195500757">
				<font SIZE="14" />
				<edge COLOR="#B2FFCC" />
			</node>
			<node ID="538D8745-6B62-443F-9816-A619A35B458C"
				TEXT="**M&#xe4;&#xdf;igung**" STYLE="bubble" FOLDED="false"
				CREATED="1514195121000" MODIFIED="1514195505285">
				<font SIZE="14" />
				<edge COLOR="#B2FFCC" />
			</node>
			<node ID="9E2B9133-DF1D-4D8F-92D9-0C9E50EF10F7"
				TEXT="**Zufriedenheit**" STYLE="bubble" FOLDED="false"
				CREATED="1514195121000" MODIFIED="1514195533916">
				<font SIZE="14" />
				<edge COLOR="#B2FFCC" />
			</node>
			<node ID="BF54B987-F494-40E3-A734-2289E7E02AE6"
				TEXT="**Opferbereitschaft**" STYLE="bubble" FOLDED="false"
				CREATED="1514195121000" MODIFIED="1514195526390">
				<font SIZE="14" />
				<edge COLOR="#B2FFCC" />
			</node>
			<node ID="B0072AD2-2C30-457D-B60F-B4A52C2C8B8F"
				TEXT="**Unterw&#xfc;rfigkeit**" STYLE="bubble" FOLDED="false"
				CREATED="1514195121000" MODIFIED="1514195537473">
				<font SIZE="14" />
				<edge COLOR="#B2FFCC" />
			</node>
			<node ID="68F4C637-F06F-4FE9-BEB6-563B1471AA63"
				TEXT="**Duldsamkeit**" STYLE="bubble" FOLDED="false"
				CREATED="1514195121000" MODIFIED="1514195541825">
				<font SIZE="14" />
				<edge COLOR="#B2FFCC" />
			</node>
			<node ID="1440FE99-3632-49E0-A601-EE5AFB8DC3DE"
				TEXT="**F&#xfc;gsamkeit**" STYLE="bubble" FOLDED="false"
				CREATED="1514195121000" MODIFIED="1514195545218">
				<font SIZE="14" />
				<edge COLOR="#B2FFCC" />
			</node>
			<node ID="EA544DE5-FAEE-4A21-96AD-41933E031EC5"
				TEXT="**Nachgiebigkeit**" STYLE="bubble" FOLDED="false"
				CREATED="1514195121000" MODIFIED="1514195548485">
				<font SIZE="14" />
				<edge COLOR="#B2FFCC" />
			</node>
			<node ID="19E61306-DE6A-40BD-9B45-29E4F935B69A"
				TEXT="**Ergebenheit**" STYLE="bubble" FOLDED="false"
				CREATED="1514195121000" MODIFIED="1514195555802">
				<font SIZE="14" />
				<edge COLOR="#B2FFCC" />
			</node>
			<node ID="0FFF51D9-82D4-47A0-A0BA-005D00444405"
				TEXT="**Hingabe**" STYLE="bubble" FOLDED="false"
				CREATED="1514195121000" MODIFIED="1514195559188">
				<font SIZE="14" />
				<edge COLOR="#B2FFCC" />
			</node>
		</node>
		<node ID="8122465C-B30B-4B92-B011-0F78E09AF137" TEXT="Heb."
			STYLE="bubble" FOLDED="false" POSITION="left" CREATED="1428765058000"
			MODIFIED="1428765058000">
			<font SIZE="62" />
			<edge COLOR="#FFB2CC" />
			<node ID="4767CF5A-795E-4131-91A0-933DA6CF40E5"
				TEXT="heb. Wurzelwort 'an&#xe1;h" STYLE="bubble" FOLDED="false"
				CREATED="1428765058000" MODIFIED="1428765058000">
				<font SIZE="29" />
				<edge COLOR="#D9FFB2" />
				<node ID="CEE10484-5A63-4CCC-BDC9-1A97D58281A3"
					TEXT="Tr&#xfc;bsal" STYLE="bubble" FOLDED="false"
					CREATED="1428765058000" MODIFIED="1428765058000">
					<font SIZE="18" />
					<edge COLOR="#D9FFB2" />
				</node>
				<node ID="68F5546B-C78E-4C8B-8C8E-DC8E717FDAF5" TEXT="Sanftmut"
					STYLE="bubble" FOLDED="false" CREATED="1428765058000"
					MODIFIED="1428765058000">
					<font SIZE="18" />
					<edge COLOR="#D9FFB2" />
				</node>
				<node ID="24104C43-A393-4320-9D83-D9E0ECAAE1CD"
					TEXT="niedergedr&#xfc;ckt sein" STYLE="bubble" FOLDED="false"
					CREATED="1428765058000" MODIFIED="1428765058000">
					<font SIZE="19" />
					<icon BUILTIN="smily_bad" />
					<edge COLOR="#D9FFB2" />
				</node>
				<node ID="BD754D46-0363-457C-B8D0-7E06FC18A568"
					TEXT="gedem&#xfc;tigt werden" STYLE="bubble" FOLDED="false"
					CREATED="1428765058000" MODIFIED="1428765058000">
					<font SIZE="19" />
					<edge COLOR="#D9FFB2" />
				</node>
				<node ID="B48509A0-5C3C-46B2-874F-BC1D66290871"
					TEXT="bedr&#xfc;ckt werden" STYLE="bubble" FOLDED="false"
					CREATED="1428765058000" MODIFIED="1428765058000">
					<font SIZE="21" />
					<edge COLOR="#D9FFB2" />
				</node>
			</node>
			<node ID="DF45887C-53A3-4FF7-B778-9D28B3EA39F1"
				TEXT="heb. kan&#xe1;" STYLE="bubble" FOLDED="false"
				CREATED="1428765058000" MODIFIED="1428765058000">
				<font SIZE="22" />
				<edge COLOR="#FFB2CC" />
				<node ID="073F191B-3DE9-4126-B856-422BD892BF0D"
					TEXT="w&#xf6;rtlich: [sich] unterwerfen" STYLE="bubble"
					FOLDED="false" CREATED="1428765058000" MODIFIED="1428765058000">
					<font SIZE="21" />
					<edge COLOR="#FFB2CC" />
				</node>
			</node>
			<node ID="6896ED2C-6C9C-4142-89EA-0966DC49EE88"
				TEXT="heb. schaph&#xe9;l" STYLE="bubble" FOLDED="false"
				CREATED="1428765058000" MODIFIED="1428765058000">
				<font SIZE="26" />
				<edge COLOR="#B2FFD9" />
				<node ID="D8A5DB16-10D6-4650-B4ED-4C03D0AD3C79"
					TEXT="w&#xf6;rtlich: niedrig sein oder werden" STYLE="bubble"
					FOLDED="false" CREATED="1428765058000" MODIFIED="1428765058000">
					<font SIZE="20" />
					<edge COLOR="#B2FFD9" />
				</node>
			</node>
			<node ID="CA27BF09-D857-4749-893B-B4AE27F5D2C6"
				TEXT="heb. hithrapp&#xe9;&#xdf;  &quot;dem&#xfc;tige dich&quot;"
				STYLE="bubble" FOLDED="false" CREATED="1428765058000"
				MODIFIED="1428765058000">
				<font SIZE="35" />
				<edge COLOR="#FFFFB2" />
				<node ID="C82B144A-4560-4F52-9A02-6396E5A257F7"
					TEXT="w&#xf6;rtlich" STYLE="bubble" FOLDED="false"
					CREATED="1428765058000" MODIFIED="1428765058000">
					<font SIZE="18" />
					<edge COLOR="#FFFFB2" />
					<node ID="9BF7E877-2F77-4070-97B7-C6DDC5403473"
						TEXT="stampfe dich nieder" STYLE="bubble" FOLDED="false"
						CREATED="1428765058000" MODIFIED="1428765058000">
						<font SIZE="38" />
						<icon BUILTIN="itmz-auction-hammer" />
						<edge COLOR="#FFFFB2" />
					</node>
				</node>
			</node>
		</node>
		<node ID="6D2C5810-59DA-406D-A09A-3F6D7778B6FD" TEXT="Gr."
			STYLE="bubble" FOLDED="false" POSITION="left" CREATED="1428765058000"
			MODIFIED="1428765058000">
			<font SIZE="62" />
			<edge COLOR="#FFCCB2" />
			<node ID="FBB1456F-AE80-4D2D-9DEC-CDE29B5E3122"
				TEXT="gr. tapeinophros&#xfd;n" STYLE="bubble" FOLDED="false"
				CREATED="1428765058000" MODIFIED="1428765058000">
				<font SIZE="28" />
				<edge COLOR="#FFCCB2" />
				<node ID="9C310F37-B683-4788-AF1F-038E1A6B163E"
					TEXT="von den W&#xf6;rtern tapein&#xf3;&#10;&quot;niedrig machen&quot; und phren  &quot;Sinn&quot; abgeleitet"
					STYLE="bubble" FOLDED="false" CREATED="1428765058000"
					MODIFIED="1428765058000">
					<font SIZE="33" />
					<edge COLOR="#FFCCB2" />
				</node>
			</node>
		</node>
		<node ID="FBE0854E-3E92-4AC8-97A6-922B960EF531" TEXT="Engl."
			LINK="https://en.wikipedia.org/wiki/Humility" STYLE="bubble"
			FOLDED="false" POSITION="left" CREATED="1514195305000"
			MODIFIED="1514195339000">
			<font SIZE="14" />
			<edge COLOR="#FFFFB2" />
			<node ID="65A85C30-96F6-492E-8CDE-17D0457F96B6" TEXT="humility"
				STYLE="bubble" FOLDED="false" CREATED="1514195312000"
				MODIFIED="1514195322000">
				<font SIZE="14" />
				<edge COLOR="#FFFFB2" />
			</node>
		</node>
		<node ID="322EDE9D-BAFC-44DF-9248-B2C5AF749D22" TEXT="Pol."
			LINK="https://pl.wikipedia.org/wiki/Pokora" STYLE="bubble"
			FOLDED="false" POSITION="left" CREATED="1514195391000"
			MODIFIED="1514195407000">
			<font SIZE="14" />
			<edge COLOR="#CCFFB2" />
			<node ID="E9B476CE-D6F2-439C-A584-A7A4A0A05967"
				TEXT="**Pokora**" STYLE="bubble" FOLDED="false"
				CREATED="1514195397000" MODIFIED="1514195415000">
				<font SIZE="14" />
				<edge COLOR="#CCFFB2" />
			</node>
		</node>
		<node ID="0DB36B91-1F98-49CF-8C06-043140903A33"
			TEXT="www.wenzlaff.info" LINK="http://www.wenzlaff.info"
			STYLE="bubble" FOLDED="false" POSITION="left" CREATED="1428765128000"
			MODIFIED="1428765128000">
			<font SIZE="10" />
			<edge COLOR="#B2B2FE" />
			<attribute NAME="Floating" VALUE="{-888, 554}" />
		</node>
	</node>
</map>