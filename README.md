# TWMindmapGenerator

Generiert Mindmap-Reports

[![pipeline status](https://gitlab.com/IT-Berater/twmindmapgenerator/badges/master/pipeline.svg)](https://gitlab.com/IT-Berater/twmindmapgenerator/commits/master)

Anleitung siehe http://blog.wenzlaff.de/?p=12022

![](http://blog.wenzlaff.de/wp-content/uploads/2018/09/FreeMarker-Template-Language-700x429.png)

```mermaid
graph TD;
Mindmap--> Freemarker; 
Mindmap--> Java; 
Mindmap--> Maven; 
Mindmap--> Template; 
```